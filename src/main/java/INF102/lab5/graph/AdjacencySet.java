package INF102.lab5.graph;

import java.util.Set;

public class AdjacencySet<V> implements IGraph<V> {

    @Override
    public int size() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void addNode(V node) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void removeNode(V node) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void addEdge(V u, V v) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void removeEdge(V u, V v) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean hasNode(V node) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean hasEdge(V u, V v) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Set<V> getNeighbourhood(V node) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean connected(V u, V v) {
        throw new UnsupportedOperationException();
    }

}
